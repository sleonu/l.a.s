﻿using UnityEngine;
using System.Collections;

public class LightPinging : MonoBehaviour
{

    float timeBetweenFlashes = 0.8f;
    float flashTime = 0.2f;
    // Use this for initialization
    void Start()
    {
        StartCoroutine("onOff");
    }

    // Update is called once per frame
    void Update()
    {
    }


    IEnumerator onOff()
    {
        while (true)
        {
            turnOffOnLights(true);
            yield return new WaitForSeconds(flashTime);
            turnOffOnLights(false);
            yield return new WaitForSeconds(timeBetweenFlashes);
        }
    }

    void turnOffOnLights(bool turnOnLight)
    {
        Transform[] ts = gameObject.GetComponentsInChildren<Transform>(true);
        if (ts == null)
            return;
        for(int i=1;i<ts.Length;i++)
        {
            ts[i].gameObject.SetActive(turnOnLight);
        }
    }
}
