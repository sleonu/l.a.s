﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class fetch_ship_pitch_roll : MonoBehaviour {
    public Transform ship;
    float x, z;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        x = ship.rotation.eulerAngles.x;
        z = ship.rotation.eulerAngles.z;

        x = (x < 350) ? x : -(360 - x);
        z = (z < 350) ? z : -(360 - z);

        GetComponent<Text>().text = "P " + x.ToString("0.00") +
                                    " / R " + z.ToString("0.00");
	}
}
