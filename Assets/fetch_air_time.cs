﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Timers;

public class fetch_air_time : MonoBehaviour {
    private System.DateTime airTime;
    private Timer airTimeTimer;

	// Use this for initialization
	void Start () {
        airTime = new System.DateTime(2016, 12, 1, 0, 0, 0);
        airTime = airTime.AddMinutes(52.3);
        Debug.Log(airTime);
        airTimeTimer = new Timer(1000);
        airTimeTimer.Elapsed += new ElapsedEventHandler(airTimeTimer_Elapsed);
        airTimeTimer.Start();
	}

    void airTimeTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
        airTime = airTime.Subtract(new System.TimeSpan(0, 0, 1));

    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = "Air Time: " + airTime.Hour + ":" + airTime.Minute + ":" + airTime.Second;
    }
}
