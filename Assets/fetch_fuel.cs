﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Timers;

public class fetch_fuel : MonoBehaviour {
    private double fuel;
    private Timer fuelTimer;

    // Use this for initialization
	void Start () {
        fuel = 64.3;
        fuelTimer = new Timer(3000);
        fuelTimer.Elapsed += new ElapsedEventHandler(fuelTimer_Elapsed);
        fuelTimer.Start();
	}

    void fuelTimer_Elapsed(object sender, ElapsedEventArgs e)
    {
        fuel -= 0.15;
    }
	
	// Update is called once per frame
	void Update () {
        GetComponent<Text>().text = "Fuel: " + System.String.Format("{0:0.00}", fuel);
	}
}
