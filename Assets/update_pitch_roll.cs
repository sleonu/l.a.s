﻿using UnityEngine;
using System.Collections;

public class update_pitch_roll : MonoBehaviour {
    public Transform ship;
    private bool isRising = true;
    private const float rotationStep = 0.4f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void FixedUpdate() {
        if (isRising)
        {
            ship.Rotate(new Vector3(Mathf.Sin(rotationStep * Mathf.PI / 180), 0, Mathf.Sin(rotationStep * Mathf.PI / 180)));
            if ((ship.rotation.eulerAngles.x >= 0.5) && (ship.rotation.eulerAngles.x <= 359.5))
                isRising = false;
        }
        else 
        {
            ship.Rotate(new Vector3(Mathf.Sin(-rotationStep * Mathf.PI / 180), 0, Mathf.Sin(-rotationStep * Mathf.PI / 180)));
            if ((ship.rotation.eulerAngles.x <= 359.5) && (ship.rotation.eulerAngles.x > 0.5))
                isRising = true;
        }
    }
}
