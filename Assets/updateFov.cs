﻿using UnityEngine;
using System.Collections;

public class updateFov : MonoBehaviour {

    public Transform Target;
    private Vector3 direction;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        direction = Target.position - transform.position;
        transform.forward = direction;


        var curFov = transform.GetComponent<Camera>().fieldOfView;
        var length = direction.magnitude;
   //     float x = 60;// = (float)(0.00667 * Mathf.Pow(length, 2) - 1.5 * length + 90);
   //     if (length <= 20)
  //          x = 60;
      
 //       else if (length > 130)
 //           x = 3;
        var norm = (length - 20) / (130 - 20);
        var x=Mathf.Lerp(60, 3, norm);
        transform.GetComponent<Camera>().fieldOfView = x;
    }

}
    

