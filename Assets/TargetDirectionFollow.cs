﻿using UnityEngine;
using System.Collections;

public class TargetDirectionFollow : MonoBehaviour {

    public Transform Target;
    private Vector3 direction;
	// Use this for initialization
	void Start ()
    {
    }
	
	// Update is called once per frame
	void Update () {
        direction = Target.position - transform.position;
        transform.forward = direction;
	}
}
