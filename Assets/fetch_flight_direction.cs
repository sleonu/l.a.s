﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class fetch_flight_direction : MonoBehaviour {
    public Transform helicopter;
    public int baseOffset = 167;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        var angle = ((int)helicopter.rotation.eulerAngles.y + baseOffset);
        GetComponent<Text>().text = "Flight dir: " + angle;
	}
}
