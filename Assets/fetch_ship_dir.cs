﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class fetch_ship_dir : MonoBehaviour {
    public Transform ship;
    public int baseOffset = 167;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        var angle = ((int)ship.rotation.eulerAngles.y + baseOffset);
        GetComponent<Image>().transform.localRotation = Quaternion.Euler(new Vector3(0, 0, angle));
	}
}
