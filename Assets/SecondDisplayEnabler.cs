﻿using UnityEngine;
using System.Collections;

public class SecondDisplayEnabler : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        Display.displays[0].Activate();
        if (Display.displays.Length > 1)
            Display.displays[1].Activate();
        else
        {
           // transform.parent.gameObject.SetActive(false);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
