﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class fetch_flight_velocity : MonoBehaviour {
    public Transform helicopter;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<Text>().text = "Flight velocity: " + (int)helicopter.transform.GetComponent<Rigidbody>().velocity.magnitude * 3;
	}
}
